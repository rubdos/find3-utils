# find3-utils

Some tools to interact with find3 on the command line.

## unifi-to-find3

`unifi-to-find3` is a utility that scrapes the
[Unifi controller](https://www.ui.com/unifi/unifi-ap/),
and puts active clients' RSSI information into FIND3 via it's `/passive` call.

Download the executable:

- [x86_64 Linux](https://gitlab.com/rubdos/find3-utils/-/jobs/artifacts/master/browse/target/release/?job=test-nightly)

### configuration

`unifi-to-find3` expects a configuration file at `/etc/unifi-to-find3.toml`:

```
[unifi]
address="https://unifi.example.com" # no trailing slashes
username="username"
password="my password."

[find3]
address="https://find3.example.com"
family="FAMILY"
```

### systemd service

I've put `unifi-to-find3` in `/usr/local/bin`.

Example `unifi-to-find3.service`
```
[Unit]
Description=unifi to find3
Requires=network-online.target

[Service]
Restart=always
RestartSec=30

ExecStart=unifi-to-find3

[Install]
WantedBy=multi-user.target
```

## Building from source

Clone the repository, [install Rust](https://rustup.rs/),
run `cargo build --release` and find the artifacts in `target/release/`.
