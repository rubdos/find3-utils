#![feature(never_type)]

use std::collections::HashMap;

use find3_utils::model::find3::*;
use find3_utils::model::unifi::*;

use failure::{Error, ensure};

use reqwest::Client;
use chrono::prelude::*;
use config::{Config, File, Value};

fn main() -> Result<!, Error> {
    // Read config
    let mut config = Config::new();
    config.merge(File::with_name("/etc/unifi-to-find3.toml"))?;
    let unifi_cfg = config.get_table("unifi")?;
    let find3_cfg = config.get_table("find3")?;

    let unifi_addr = unifi_cfg.get("address").unwrap().clone().into_str()?;
    let username = unifi_cfg.get("username").unwrap().clone().into_str()?;
    let password = unifi_cfg.get("password").unwrap().clone().into_str()?;
    let unifi_site = unifi_cfg.get("site")
        .unwrap_or(&Value::from("default")).clone().into_str()?;

    let find3_addr = find3_cfg.get("address").unwrap().clone().into_str()?;
    let family = find3_cfg.get("family").unwrap().clone().into_str()?;

    let unifi_client = Client::builder()
        .cookie_store(true)
        .build()?;
    let login_path = format!("{base}/api/login", base=unifi_addr);
    let clients_path = format!("{base}/api/s/{site}/stat/sta",
        base=unifi_addr, site=unifi_site);

    let find3_client = Client::new();
    let passive_path = format!("{}/passive", find3_addr);

    let credentials = Login {
        username,
        password,
        remember: Some(true),
    };
    let login_result: UnifiResponse<()> = unifi_client.post(&login_path)
        .json(&credentials)
        .send()?
        .json()?;

    ensure!(login_result.meta.rc == UnifiRc::Ok,
        "login failed: {:?}, credentials: {:?}", login_result, credentials);

    loop {
        let mut result: UnifiResponse<ClientAssociation> = unifi_client
            .get(&clients_path)
            .send()?
            .json()?;

        println!("Client count: {}", result.data.len());
        result.data.retain(|assoc| {
            !assoc.is_wired
        });
        println!("Wireless client count: {}", result.data.len());

        if result.data.is_empty() {
            std::thread::sleep(std::time::Duration::from_secs(30));
            continue;
        }

        // We need to aggregate the location data *per base station*.

        let mut find3_data: HashMap<String, Find3Data> = HashMap::new();

        for client in &result.data {
            let ap_mac = client.ap_mac.as_ref().unwrap();
            println!("Client {} at AP: {}", client.mac, ap_mac);
            let entry = find3_data.entry(ap_mac.to_string())
                .or_insert(Find3Data {
                    device: format!("unifi-scraper-{}", ap_mac),
                    family: family.clone(),
                    time: Utc::now(),
                    location: None,
                    gps: None,
                    sensor_data: SensorData {
                        bluetooth: HashMap::new(),
                        wifi: HashMap::new(),
                        extra: HashMap::new(),
                    },
                }
            );

            entry.sensor_data.wifi.insert(
                client.mac.to_string(),
                client.rssi.expect("wireless without RSSI") as i16);
        }

        for entry in find3_data {
            let result: Find3Response = find3_client.post(&passive_path)
                .json(&entry)
                .send()?
                .json()?;

            ensure!(result.success, "Posting data failed: {}", result.message);
        }

        std::thread::sleep(std::time::Duration::from_secs(5));
    }
}
