use serde::{Serialize, Deserialize};
use std::collections::HashMap;
use chrono::prelude::*;

#[derive(Debug, Serialize, Deserialize)]
pub struct Login {
    pub username: String,
    pub password: String,
    pub remember: Option<bool>,
}

#[derive(Eq, PartialEq, Debug, Serialize, Deserialize)]
#[serde(deny_unknown_fields)]
pub enum UnifiRc {
    #[serde(rename = "ok")]
    Ok,
    #[serde(rename = "error")]
    Error,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct UnifiMeta {
    pub rc: UnifiRc,
    pub msg: Option<String>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct UnifiResponse<Data> {
    pub meta: UnifiMeta,
    pub data: Vec<Data>,
}

// TODO: extract wireless and wired information in two separate structs,
// serialize and deserialize either one of them.
#[derive(Debug, Serialize, Deserialize)]
pub struct ClientAssociation {
    pub site_id: String,
    #[serde(with="chrono::serde::ts_seconds")]
    pub assoc_time: DateTime<Utc>,
    #[serde(with="chrono::serde::ts_seconds")]
    pub latest_assoc_time: DateTime<Utc>,
    pub oui: String,
    pub user_id: String,
    pub _id: String,
    pub mac: String,
    pub is_guest: bool,
    #[serde(with="chrono::serde::ts_seconds")]
    pub first_seen: DateTime<Utc>,
    #[serde(with="chrono::serde::ts_seconds")]
    pub last_seen: DateTime<Utc>,
    pub is_wired: bool,
    pub hostname: Option<String>,
    pub name: Option<String>,
    /// Uptime of in seconds
    pub uptime: u32,
    pub ap_mac: Option<String>,
    pub channel: Option<u16>,
    pub radio: Option<String>, // XXX: enum
    pub radio_name: Option<String>,
    pub essid: Option<String>,
    pub bssid: Option<String>,
    pub powersave_enabled: Option<bool>,
    pub rssi: Option<i32>,
    pub noise: Option<i32>,
    pub signal: Option<i32>,
    pub tx_rate: Option<u32>,
    pub rx_rate: Option<u32>,
    pub is_11r: Option<bool>,

    #[serde(flatten)]
    pub extra: HashMap<String, serde_json::Value>,
}
