use std::collections::HashMap;

use chrono::prelude::*;

use serde::{Serialize, Deserialize};

#[derive(Debug, Serialize, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct Find3Data {
    #[serde(rename = "d")]
    pub device: String,
    #[serde(rename = "f")]
    pub family: String,
    #[serde(rename = "t", with = "chrono::serde::ts_seconds")]
    pub time: DateTime<Utc>,
    #[serde(rename = "l")]
    pub location: Option<String>,

    pub gps: Option<GpsData>,
    #[serde(rename = "s")]
    pub sensor_data: SensorData,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(deny_unknown_fields)]
/// Represents the location on earth, where this data was measured.
pub struct GpsData {
    /// The latitude coordinate.
    pub lat: f32,
    /// The longitude coordinate.
    pub lon: f32,
    /// The altitude measurement in whatever unit FIND3 wants.
    pub alt: f32,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(deny_unknown_fields)]
/// Struct holding all the measurement points.
///
/// # Example:
/// ```
/// use find3_utils::model::find3::*;
/// let sd: SensorData = serde_json::from_str("{ \"bluetooth\": {} }").unwrap();
/// ```
pub struct SensorData {
    #[serde(default)]
    pub bluetooth: HashMap<String, i16>,
    #[serde(default)]
    pub wifi: HashMap<String, i16>,

    #[serde(flatten)]
    pub extra: HashMap<String, HashMap<String, i16>>,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct Find3Response {
    pub message: String,
    pub success: bool,
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn deserialize_the_example() {
        let example = include_str!("find3-post-data.json");
        let _deserialized: Find3Data = serde_json::from_str(example).unwrap();
    }
}
